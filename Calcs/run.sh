#!/bin/bash

dir=$(pwd)

cd "$dir/Ambient/BandStructure/Cubic/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log

cd "$dir/Pressure/Cubic/0.25GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log

cd "$dir/Pressure/Cubic/0.94GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log


cd "$dir/Pressure/Cubic/1.22GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log

cd "$dir/Ambient/BandStructure/SCAN2"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log


cd "$dir/Pressure/0.25GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log

cd "$dir/Pressure/0.55GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log

cd "$dir/Pressure/0.94GPa/SCAN"
echo $(pwd)
echo "mpirun -n 3 abinit < files >& log"
mpirun -n 3 abinit < files >& log
