#!/bin/bash

cd 0.25GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files >& log"
mpirun -n 8 abinit < files >& log

cd ../0.94GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files >& log"
mpirun -n 8 abinit < files >& log


cd ../1.22GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files >& log"
mpirun -n 8 abinit < files >& log


cd ../1.71GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files >& log"
mpirun -n 8 abinit < files >& log

cd ../../../Ambient/BandStructure/Cubic
echo $(pwd)
echo "mpirun -n 8 abinit < files >& log"
mpirun -n 8 abinit < files >& log



