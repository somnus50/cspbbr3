#!/bin/bash

FILE="$1"

while read LINE; do
   #echo "$LINE"
   GPA=${LINE%%\ *}
   LATT=${LINE#*\ }
   echo "GPa: $GPA Latt: $LATT"

   sed "s/mylat/$LATT/g" CsPbBr3_cubic.inp > temp.inp

echo -e "temp.inp
ab_out_$GPA-GPA
abi_$GPA-GPA
abo_$GPA-GPA
tmp_$GPA-GPA
Cs.psp8
Pb.psp8
Br.psp8
" > files2
date
echo "Running abinit"
mpirun -n 8 abinit < files2 >& "log_$GPA-GPa"
date
echo "------------"

done < "$FILE"

rm -f files2 temp.inp

