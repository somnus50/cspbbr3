#!/bin/bash

FILE="$1"

while read LINE; do
   #echo "$LINE"

   arr=($LINE)

   #echo ${arr[@]}
   #for i in ${arr[@]}; do echo $i; done 

   GPA=${arr[0]}
   LATTA=${arr[1]}
   LATTB=${arr[2]}
   LATTC=${arr[3]}
   echo "GPa: $GPA Latt: $LATTA $LATTB $LATTC"

   sed -e "s/mylatta/$LATTA/g;s/mylattb/$LATTB/g;s/mylattc/$LATTC/g;" CsPbBr3.inp > temp.inp

echo -e "temp.inp
ab_out_$GPA-GPA
abi_$GPA-GPA
abo_$GPA-GPA
tmp_$GPA-GPA
Cs.psp8
Pb.psp8
Br.psp8
" > files2
date
echo "Running abinit"
mpirun -n 8 abinit < files2 >& "log_$GPA-GPa"
date
echo "------------"

done < "$FILE"

rm -f files2 temp.inp

