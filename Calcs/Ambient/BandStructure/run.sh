#!/bin/bash


echo $(pwd)
echo "mpirun -n 8 abinit < files2 >& log2"
mpirun -n 8 abinit < files2 >& log2

cd ../../Pressure/0.25GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files2 >& log2"
mpirun -n 8 abinit < files2 >& log2

cd ../0.55GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files2 >& log2"
mpirun -n 8 abinit < files2 >& log2

cd ../0.94GPa
echo $(pwd)
echo "mpirun -n 8 abinit < files2 >& log2"
mpirun -n 8 abinit < files2 >& log2
